#pragma once
//#include "Game.h"

class Game;

class NameMode
{
public:
	NameMode() :mpGame(nullptr) {}
	//setup
	void Init(Game*);
	//handle any logic
	void Update() {};
	//display
	void Render();
	//process windows text messages
	void TextEntered(char);
private:
	Game *mpGame;	//the only way to communicate with the rest of the game
};





