#pragma once
#include "SFML/Graphics.hpp"

struct Vec2i
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Vec2f
{
	float x, y;
};

const float PI = 3.14159265358979323846f;
inline float Deg2Rad(float deg) {
	return deg * (PI / 180.f);
}

class GameObj
{
public:
	GameObj() : mVel{ 0, 0 } {};
	void Update();
	void Render(sf::RenderWindow& window) {
		window.draw(mSprite);
	}
	void SetTexture(const sf::Texture& t) {
		mSprite.setTexture(t, true);
	}
	void SetTexture(const sf::Texture& t, const sf::IntRect& rect) {
		mSprite.setTexture(t);
		mSprite.setTextureRect(rect);
	}
	void SetOrigin(const Vec2f& off) {
		mSprite.setOrigin(sf::Vector2f(off.x, off.y));
	}
	void SetScale(const Vec2f& s) {
		mSprite.setScale(s.x, s.y);
	}
	Vec2f GetScale() {
		return Vec2f{ mSprite.getScale().x, mSprite.getScale().y };
	}
	Vec2f GetPos() {
		return Vec2f{ mSprite.getPosition().x,mSprite.getPosition().y };
	}
	void SetPos(const Vec2f& pos) {
		mSprite.setPosition(pos.x, pos.y);
	}
	float GetDegrees() {
		return mSprite.getRotation();
	}
	void SetDegrees(float angle) {
		mSprite.setRotation(angle);
	}
	void AddRotation(float angle) {
		mSprite.rotate(angle);
	}
	void SetVel(const Vec2f& v) {
		mVel.x = v.x;
		mVel.y = v.y;
	}
	const Vec2f GetVel() const {
		return Vec2f{ mVel.x,mVel.y };
	}
	Vec2f GetDim() const {
		return Vec2f{ (float)mSprite.getTextureRect().width, (float)mSprite.getTextureRect().height };
	}

private:
	sf::Sprite mSprite; //image and position
	sf::Vector2f mVel;	//velocity
};