#pragma once
#include "Wall.h"
#include "Gun.h"

class Game;
class Wall;

class PlayMode
{
public:
	PlayMode() : mpGame(nullptr) {}
	void Init(Game*);
	void Update();
	void Render();
private:
	Game *mpGame; //for communication

	sf::Texture mCannonTex;			//cannon and ball
	sf::Texture mWallTex;			//walls
	Wall mWalls[Wall::MAX_WALLS];	//four walls
	Gun mGun;		//cannon
};